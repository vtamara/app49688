Rails.application.routes.draw do
  scope "/a" do
    resources :users
    mount Engine49688::Engine => "/", as: :engine49688
  end
end
