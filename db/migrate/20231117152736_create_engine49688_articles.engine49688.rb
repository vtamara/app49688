# This migration comes from engine49688 (originally 20231117152718)
class CreateEngine49688Articles < ActiveRecord::Migration[7.0]
  def change
    create_table :engine49688_articles do |t|
      t.string :title
      t.text :text

      t.timestamps
    end
  end
end
