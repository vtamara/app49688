require "test_helper"
class RoutesTest < ActionDispatch::IntegrationTest
  test "users_path" do
    assert_equal '/a/a/users', main_app.users_path
    get main_app.users_path
    assert_response :success
  end

  test "articles_path" do
    assert_equal '/a/a/articles', engine49688.articles_path
    get engine49688.articles_path
    assert_response :success
  end
end
