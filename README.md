# Application that includes an engine and shows error https://github.com/rails/rails/issues/49688

Minimal application to illustrate the problem https://github.com/rails/rails/issues/49688 by using the 
minimal engine https://gitlab.com/vtamara/engine49688

Based on the application for the blorgh example of https://edgeguides.rubyonrails.org/engines.html.

# Preparing

For simplicity we use sqlite3.

To use:
```sh
    git clone https://gitlab.com/vtamara/app49688.git`
    cd app49688
    bundle
    bin/rails db:setup
```
The last step will prepare the database in `db/development.sqlite3`

# Passing a test with rails 7.0

There is a simple test case that you can run with:
```sh
    bin/rails test test/controllers/routes_test.rb
```

This test pass using the default configuration with rails 7.0:
```
Running 1 tests in a single process (parallelization threshold is 50)
Run options: --seed 22764

# Running:

.

Finished in 0.345284s, 2.8962 runs/s, 2.8962 assertions/s.
1 runs, 1 assertions, 0 failures, 0 errors, 0 skips
```

# Upgrading to rails 7.1 and see the same test failing

Change to rails 7.1 by editing the `Gemfile` and replacing the line 
```ruby
    gem "rails", "~> 7.0.8"
```
with
```ruby
    gem "rails", "~> 7.1"
```
then run
```sh
    bundle update
    bundle
```
And then running the same test will fail:

```
 % bin/rails test test/controllers/routes_test.rb
...
Running 1 tests in a single process (parallelization threshold is 50)
Run options: --seed 2957

# Running:

F

Failure:
RoutesTest#test_articles_path [/home/vtamara/comp/rails/tmp/app49688/test/controllers/routes_test.rb:6]:
Expected: "/a/articles"
  Actual: "/a/a/articles"


bin/rails test test/controllers/routes_test.rb:5



Finished in 0.206910s, 4.8330 runs/s, 4.8330 assertions/s.
1 runs, 1 assertions, 1 failures, 0 errors, 0 skips
```

If you need to go back to rails 7.0 in the Gemfile leave again the line:
```ruby
    gem "rails", "~> 7.0.8"
```
And downgrade by doing:
```sh
    rm Gemfile.lock
    bundle
```


# How did we generate this application?

Before this application we generated the engine as described at <https://gitlab.com/vtamara/engine49688/-/blob/main/README.md>

We used OpenBSD/adJ 7.4a1 that includes ruby 3.2.2 and sqlite 3.42.0 and installed rails 7.0 globally (by running `doas gem uninstall rails; doas gem install rails --version 7.0.8`) and followed instructions from <https://edgeguides.rubyonrails.org/engines.html> like this:

```sh
rails new app49688 --minimal --skip-keeps
cd app49688
```
In the `Gemfile` we added:
```ruby
  gem "engine49688", git: "https://gitlab.com/vtamara/engine49688/"
```
and to make easier transtion between rails 7.0 and 7.1 we removed `, "~> 5.0"` from the line:
```ruby
  gem "puma", "~> 5.0"
```
Then we run
```sh
   bundle
   bin/rails engine49688:install:migrations
   bin/rails g scaffold user username:string name:string
   bin/rails db:migrate
```
and changed `config/routes.rb` to be:

```ruby
Rails.application.routes.draw do            
  scope "/a" do
    resources :users
  end
  mount Engine49688::Engine => "/a", as: :engine49688
end
```
Then we edited `config/application.rb` to remove `config.load_defaults 7.0` (making easir to upgrade/downgrade from 7.0 to 7.1) and to add:

```ruby
   config.relative_url_root = "/a/"
``` 

Finally we created the test `test/controllers/routes_test.rb` with:
```ruby
require "test_helper"
class RoutesTest < ActionDispatch::IntegrationTest

  test "articles_path" do
    assert_equal '/a/articles', engine49688.articles_path
  end
end
```
